import os
import json
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scrapy_test.spiders.costco_details import CostcoDetailsSpider
from scrapy_test.spiders.aliexress_details import AliexpressDetailsSpider


os.environ["SCRAPY_SETTINGS_MODULE"] = "scrapy_test.settings"


if __name__ == '__main__':
    process = CrawlerProcess(get_project_settings())

    # # 100357903 100685102
    # products = json.dumps(["100357903", "100685102", "100671194"])
    # process.crawl(CostcoDetailsSpider, products)
    # process.start()

    # 1005001836048772 1005001849109111 1005001874259814
    products = json.dumps(["1005001836048772"])
    process.crawl(AliexpressDetailsSpider, products, country_code="CA")
    process.start()
