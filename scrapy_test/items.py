import re
import scrapy


def to_float(number: str):
    try:
        return float(number.replace(",", ""))
    except:
        return


def to_int(number: str):
    try:
        return int(number)
    except:
        return


def format_price(price_str: str):
    non_decimal = re.compile(r'[^\d. -]+')
    return non_decimal.sub("", price_str).strip()


class RootTestItem(scrapy.Item):
    id = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    in_stock_qty = scrapy.Field()
    description = scrapy.Field()

    variations = scrapy.Field()
    all_images = scrapy.Field()


class CostcoTestItem(RootTestItem):
    before_deal_price = scrapy.Field()
    features = scrapy.Field()

    shipping = scrapy.Field()


class AliItem(RootTestItem):
    min_price = scrapy.Field()
    max_price = scrapy.Field()

    sold_by = scrapy.Field()
    item_input_id = scrapy.Field()
    store_name = scrapy.Field()
    brand = scrapy.Field()
    manufacturer = scrapy.Field()

    country = scrapy.Field()
    seller_id = scrapy.Field()
    desc_url = scrapy.Field()
