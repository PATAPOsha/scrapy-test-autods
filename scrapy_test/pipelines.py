# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import lxml.html.clean as clean

class ScrapyTestPipeline:
    fields_to_clean = ["description", "features"]
    fields_to_pop = ["before_deal_price", "desc_url", "seller_id", "currency"]

    safe_attrs = ['title']
    kill_tags = ['a', 'img', 'object', 'iframe']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cleaner = clean.Cleaner(safe_attrs_only=True, safe_attrs=self.safe_attrs, kill_tags=self.kill_tags)

    def process_item(self, item, spider):
        for field in self.fields_to_clean:
            value = item.get(field)
            if value:
                clean_val = self.cleaner.clean_html(value)
                clean_val = clean_val.replace("\n", "").replace("\t", "").replace("\r", "")
                item[field] = clean_val

        item_id = item.pop("id")
        for field in self.fields_to_pop:
            item.pop(field, None)

        out_item = {item_id: item}
        return out_item
