import re
import json
import scrapy
import base64
import logging
from urllib.parse import urlencode


from scrapy_test import items


COUNTRY_COOKIES = {
    "CA": {
        "cookies": {
            "aep_usuc_f": "region=CA&site=glo&b_locale=en_US&c_tp=CAD"
        }
    },
    "US": {
        "cookies": {
            "aep_usuc_f": "site=glo&region=US&b_locale=en_US&c_tp=USD"
        }
    },
    "UA": {
        "cookies": {
            "aep_usuc_f": "region=UA&site=glo&b_locale=en_US&c_tp=UAH"
        }
    }
}


class AliexpressDetailsSpider(scrapy.Spider):
    name = "ali_details"

    def __init__(self, product_ids, country_code="CA", *args, **kwargs):
        """
        :param product_ids: json-serialized list of product ids. E.x. ["1005001836048772", ...]
        """
        super().__init__(*args, **kwargs)

        self.base_url = "https://www.aliexpress.com/"
        self.product_ids = json.loads(product_ids)
        self.country_code = country_code

        if country_code not in COUNTRY_COOKIES:
            raise NotImplementedError(f"Unsupported country_code: {country_code}")
        self.locale_cookies = COUNTRY_COOKIES[country_code]["cookies"]

    @staticmethod
    def parse_details(resp: scrapy.http.HtmlResponse):
        item = items.AliItem()

        json_data = re.search(r'data: ({"actionModule":.*}}),', resp.text)
        if not json_data:
            raise ValueError("Cannot parse json_data")

        json_data = json_data.group(1)
        json_data = json.loads(json_data)

        item["id"] = resp.meta["product_id"]
        item["item_input_id"] = str(item["id"])
        item["url"] = resp.url
        item["title"] = json_data["pageModule"]["title"]
        item["desc_url"] = json_data["descriptionModule"]["descriptionUrl"]

        item["min_price"] = json_data["priceModule"]["minActivityAmount"]["value"]
        item["max_price"] = json_data["priceModule"]["maxActivityAmount"]["value"]
        item["price"] = min(item["min_price"], item["max_price"])

        brand = ""
        for prop in json_data.get("specsModule", {}).get("props", []):
            if prop.get("attrName") == "Brand Name":
                brand = prop.get("attrValue", "")
                break
        item["brand"] = brand

        item["sold_by"] = json_data.get("storeModule", {}).get("storeName")
        item["in_stock_qty"] = json_data.get("quantityModule", {}).get("totalAvailQuantity") or json_data.get("actionModule", {}).get("totalAvailQuantity")
        item["currency"] = json_data.get("webEnv", {}).get("currency") or json_data.get("commonModule", {}).get("currencyCode")
        item["seller_id"] = json_data.get("commonModule", {}).get("sellerAdminSeq")

        options = dict()
        variations = list()
        all_images = set()

        for image in json_data.get("imageModule", {}).get("imagePathList", []):
            all_images.add(image)

        for sku_property in json_data.get("skuModule", {}).get("productSKUPropertyList", []):
            for property_value in sku_property.get("skuPropertyValues", []):
                img = property_value.get("skuPropertyImagePath", "")
                options[property_value["propertyValueId"]] = {
                    "type": sku_property["skuPropertyName"],
                    "value": property_value["propertyValueName"],
                    "img": img
                }
                if img:
                    all_images.add(img)

        for sku_item in json_data.get("skuModule", {}).get("skuPriceList", []):
            variation = {
                "item_input_id": f"{item['id']}_{sku_item['skuPropIds']}",
                "price": sku_item["skuVal"]["skuActivityAmount"]["value"],
                "in_stock_qty": sku_item["skuVal"]["availQuantity"],
                "url": item["url"],
                "in_stock": sku_item["skuVal"]["isActivity"],
                "attributes": {}
            }
            for prop_id in sku_item["skuPropIds"].split(","):
                prop_id = int(prop_id)
                if prop_id in options:
                    variation["attributes"][options[prop_id]["type"]] = options[prop_id]["value"]
                    variation["image_url"] = options[prop_id]["img"]
            variations.append(variation)

        item["all_images"] = list(all_images)
        item["variations"] = variations
        return item


    def start_requests(self):
        for product_id in self.product_ids:
            url = f"{self.base_url}item/{product_id}.html"
            yield scrapy.Request(
                url=url,
                callback=self.process_details,
                cookies=self.locale_cookies,
                meta={
                    "product_id": product_id
                }
            )

    def process_details(self, response: scrapy.http.HtmlResponse):
        item = self.parse_details(response)
        response.meta["item"] = item

        desc_url = item["desc_url"]
        if desc_url:
            yield self.request_description(desc_url, response)
        elif item.get("seller_id"):
            yield self.request_shipping(response)
        else:
            yield item

    def request_description(self, url, response: scrapy.http.HtmlResponse):
        return scrapy.Request(
            url=url,
            callback=self.process_description,
            headers={
                "Origin": self.base_url,
                "Referer": self.base_url,
                "Accept": "application/json, text/plain, */*"
            },
            meta=response.meta
        )

    def request_shipping(self, response: scrapy.http.HtmlResponse):
        meta = response.meta
        item = meta["item"]

        seller_id = item.get("seller_id")
        if seller_id:
            params = {
                "productId": meta["product_id"],
                "count": "1",
                "minPrice": item["min_price"],
                "maxPrice": item["max_price"],
                "country": self.country_code,
                "provinceCode": "",
                "cityCode": "",
                "tradeCurrency": item["currency"],
                "sellerAdminSeq": seller_id,
                "userScene": "PC_DETAIL_SHIPPING_PANEL",
                "displayMultipleFreight": "false",
            }
            url = f"{self.base_url}aeglodetailweb/api/logistics/freight?" + urlencode(params)
            return scrapy.Request(
                url=url,
                callback=self.process_shipping,
                headers={
                    "Referer": item["url"],
                    "Accept": "application/json, text/plain, */*"
                },
                meta=meta
            )

        else:
            return item

    def process_description(self, response: scrapy.http.HtmlResponse):
        item = response.meta["item"]
        item["description"] = response.text

        if item.get("seller_id"):
            yield self.request_shipping(response)
        else:
            yield item

    def process_shipping(self, response: scrapy.http.HtmlResponse):
        item = response.meta["item"]
        shipping = []

        try:
            json_resp = json.loads(response.text)
        except Exception as e:
            logging.exception(f"[process_shipping] Failed to parse json:\n{e}")
            yield item
            return

        for result in json_resp.get("body", {}).get("freightResult", []):
            shipping_option = {
                "shipping_tag": result["company"],
                "country": self.country_code,
                "currency": result["freightAmount"]["currency"],
                "tracking_number": result["tracking"],
                "shipping_price": result["freightAmount"]["value"],
                "shipping_time": result["time"]
            }
            shipping.append(shipping_option)

        for variation in item["variations"]:
            variation["shipping"] = shipping

        yield item


