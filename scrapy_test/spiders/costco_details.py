import re
import json
import scrapy
import base64
import logging
from urllib.parse import urlencode


from scrapy_test import items


class CostcoDetailsSpider(scrapy.Spider):
    name = "costco_details"

    def __init__(self, product_ids, *args, **kwargs):
        """
        :param product_ids: json-serialized list of product ids. E.x. ["100357903", ...]
        """
        super().__init__(*args, **kwargs)

        self.base_url = "https://www.costco.com/"
        self.product_ids = json.loads(product_ids)

    @staticmethod
    def parse_details(resp: scrapy.http.HtmlResponse):
        item = items.CostcoTestItem()

        item["id"] = resp.meta["product_id"]
        item["url"] = resp.url
        item["title"] = resp.xpath('//span[has-class("product-title")]/text()').get()
        item["description"] = resp.xpath('//div[has-class("product-info-description")]').get()
        item["features"] = resp.xpath('//ul[has-class("pdp-features")]').get()
        item["currency"] = resp.css("meta[property='product:price:currency']::attr(content)").get()

        price = re.search(r"priceMin: '(.*)',", resp.text)
        if price:
            price = price.group(1)
            item["price"] = items.to_float(price)

        before_deal_price = re.search(r"priceMax: '(.*)'", resp.text)
        if before_deal_price:
            before_deal_price = before_deal_price.group(1)
            item["before_deal_price"] = items.to_float(before_deal_price)

        in_stock_qty = re.search(r"MAX_ITEM_QUANTITY : (\d+)", resp.text)
        if in_stock_qty:
            in_stock_qty = in_stock_qty.group(1)
            item["in_stock_qty"] = items.to_int(in_stock_qty)

        return item

    @staticmethod
    def parse_variations(item, resp: scrapy.http.HtmlResponse):
        options = dict()
        variations = list()

        variations_data = re.search(r'var products = \[([\s\S]*)\n];[\s\S]*var options', resp.text)
        if variations_data:
            variations_data = variations_data.group(1)
            variations_data = json.loads(variations_data)

        options_data = re.search(r'var options = \[([\s\S]*)\n];', resp.text)
        if options_data:
            options_data = options_data.group(1)
            options_data = options_data.replace("'", '"')
            options_data = json.loads(options_data)

        if isinstance(options_data, list) and len(options_data):
            for option in options_data:
                for key, val in option.get("v", {}).items():
                    options[key] = {
                        "type": option.get("n", ""),
                        "value": val
                    }

        for variation in variations_data:
            variation_price = items.to_float(base64.b64decode(variation.get("price")).decode())
            attributes = dict()
            for option in variation.get("options"):
                if option in options:
                    attributes[options[option]["type"]] = options[option]["value"]

            variations.append({
                "price": variation_price or item["price"],
                "before_deal_price": item["before_deal_price"],
                "image_url": variation.get("img_url"),
                "in_stock": int(variation.get("inventory") == "IN_STOCK"),
                "in_stock_qty": variation.get("maxQty") or item["in_stock_qty"],
                "url": item["url"],
                "attributes": attributes,
                "is_main_variation": len(variations_data) == 1,
                "shipping": item["shipping"]
            })
        item["variations"] = variations
        return item

    @staticmethod
    def parse_shipping(item, resp: scrapy.http.HtmlResponse):
        shipping_options = []

        # Didn't find any other shipping option example except of free shipping
        if re.search(r"Shipping & Handling Included\*", resp.text):
            shipping = {
                "free_shipping": 1,
                "shipping_price": 0,
                "currency": item.get("currency"),
                "tracking_number": bool(re.search(r"Tracking #", resp.text)),
                "country": "USA"
            }
            shipping_time = re.search(r"estimated delivery.*(\d) business day", resp.text)
            if shipping_time:
                try:
                    shipping_time = int(shipping_time.group(1))
                    shipping["shipping_time"] = shipping_time
                except:
                    pass
            shipping_options.append(shipping)

        item["shipping"] = shipping_options
        return item

    @staticmethod
    def gen_images_url(resp: scrapy.http.HtmlResponse):
        # extra request to get images

        profile_id = re.search(r'profileId=(\d+)&', resp.text)
        item_id = re.search(r'&itemId=(.*)&', resp.text)

        if not profile_id or not item_id:
            return

        profile_id = profile_id.group(1)
        item_id = item_id.group(1)

        images_url = "https://richmedia.ca-richimage.com/ViewerDelivery/productXmlService"
        params = {
            "profileid": profile_id,
            "itemid": item_id,
            "viewerid": "1067",
            "callback": "productXmlCallbackImagePanZoomprofile"
        }
        images_url += "?" + urlencode(params)
        return images_url

    def start_requests(self):
        for product_id in self.product_ids:
            url = f"https://www.costco.com/.product.{product_id}.html?&ADBUTLERID=grocery_itemdriver_filet"
            yield scrapy.Request(
                url=url,
                callback=self.process_details,
                meta={
                    "product_id": product_id
                }
            )

    def process_details(self, response: scrapy.http.HtmlResponse):
        meta = response.meta

        item = self.parse_details(response)
        item = self.parse_shipping(item, response)
        item = self.parse_variations(item, response)

        meta["item"] = item

        images_url = self.gen_images_url(response)

        if not images_url:
            logging.error(f"[{meta['product_id']}] Failed to get images.")
            yield item
            return

        yield scrapy.Request(
            url=images_url,
            callback=self.process_images,
            meta=meta
        )

    def process_images(self, response: scrapy.http.HtmlResponse):
        item = response.meta["item"]
        images = []

        json_text = response.text.replace("productXmlCallbackImagePanZoomprofile(", "")
        json_resp = json.loads(json_text[:-1])

        for img in json_resp.get("product", {}).get("images", []):
            images.append(img["@path"])

        item["all_images"] = images
        yield item
